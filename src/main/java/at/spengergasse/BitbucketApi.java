package at.spengergasse;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
/*

eine exception trat bei bitbucket auf die bei keiner anderen api kam, dehr hab ich github auch verwendet.
Github api funktioniert!
 */
public class BitbucketApi {

    private static HttpClient http = HttpClient.newHttpClient();

    private static HttpRequest requestReposOfUsers(String user) {
        return HttpRequest.newBuilder().uri(URI.create("https://api.bitbucket.org/2.0/repositories/" + user)).build();
    }

    public static void getReposOfUsers(List<String> users) {
        Long timeBeforeSequentialRequest = System.currentTimeMillis();
        users.stream()
                .sequential()
                .forEach(user -> http.sendAsync(requestReposOfUsers(user), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenAccept(System.out::println)
                        .join()
                );
        Long timeBeforeParallelRequest = System.currentTimeMillis();
        users.stream()
                .parallel()
                .forEach(user -> http.sendAsync(requestReposOfUsers(user), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenAccept(System.out::println)
                        .join()
                );
        Long timeAfterParallelRequest = System.currentTimeMillis();
        System.out.println("Sequential Request took " + (timeBeforeParallelRequest - timeBeforeSequentialRequest) + " Milliseconds");
        System.out.println("Parallel Request took " + (timeAfterParallelRequest - timeBeforeParallelRequest) + " Milliseconds");

    }


    public static void main(String[] args) {
        List<String> useres = new ArrayList<String>();
        useres.add("BernhardBalik");
        useres.add("BenjaminChihungi");
        useres.add("RaphaelStella");
        useres.add("kadlec-spengergasse");
        getReposOfUsers(useres);

    }

}
